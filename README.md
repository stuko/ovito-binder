# OVITO Binder

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ovito-org%2Fovito-binder/HEAD)

A collection of Jupyter notebooks to demonstrate the use of the [Viewport.create_jupyter_widget()](https://docs.ovito.org/python/modules/ovito_vis.html#ovito.vis.Viewport.create_jupyter_widget) function from the [OVITO Python Programming Interface](https://docs.ovito.org/python/).

## Notebooks

* [Interactive viewport demo](https://mybinder.org/v2/gl/ovito-org%2Fovito-binder/HEAD?labpath=viewport.ipynb)

* [Trajectory viewer demo](https://mybinder.org/v2/gl/ovito-org%2Fovito-binder/HEAD?labpath=trajectory_viewer.ipynb)

## Technical information

The repository files [`environment.yml`](binder/environment.yml), [`apt.txt`](binder/apt.txt) and [`start`](binder/start) are the [Binder configuration files](https://mybinder.readthedocs.io/en/latest/using/config_files.html) that set up the Jupyter environment. In addition to installing the `ovito` Anaconda package and its dependencies, they start a virtual X display server with OpenGL support to enable visual viewport windows in Jupyter notebooks.