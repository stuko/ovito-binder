import os
#os.environ['OVITO_GUI_MODE'] = '1' # Request a session with OpenGL support (required for ambient occlusion modifier)

import ovito
from ovito.data import *
from ovito.io import *
from ovito.vis import * 
from ovito.modifiers import * 
from ovito.pipeline import *
import json
from ipywidgets.embed import embed_data
import ipywidgets

def export_widgets(fname, widgets):
    data = embed_data(views=widgets)
    html_template = """
    <html>
      <head>

        <title>OVITO VR Demo</title>

        <!-- Load RequireJS, used by the IPywidgets for dependency management -->
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"
            integrity="sha256-Ae2Vz/4ePdIu6ZyI/5ZGsYnb+m0JlOmKPjt6XZ9JJkA="
            crossorigin="anonymous">
        </script>

        <!-- Load IPywidgets bundle for embedding. -->
        <script
            data-jupyter-widgets-cdn="https://unpkg.com/"
            data-jupyter-widgets-cdn-only
            src="https://cdn.jsdelivr.net/npm/@jupyter-widgets/html-manager@*/dist/embed-amd.js"
            crossorigin="anonymous">
        </script>

        <!-- The state of all the widget models on the page -->
        <script type="application/vnd.jupyter.widget-state+json">
          {manager_state}
        </script>
      </head>

      <body>
        <h1>OVITO VR Demo</h1>
        <div style="display: flex; flex-wrap: wrap;">
        {divs}
        </div>
        <p>Click "VR" button to start virtual reality mode (requires a WebXR capable device).</p>
      </body>
    </html>
    """

    div_template = """
    <div id="viewport-widget-{index}" style="margin: 10px;">
          <script type="application/vnd.jupyter.widget-view+json">
            {widget_view}
          </script>
        </div><br>
    """
    manager_state = json.dumps(data['manager_state'])
    widget_views = [json.dumps(view) for view in data['view_specs']]
    divs = [div_template.format(index=i, widget_view=wv) for i,wv in enumerate(widget_views)]
    rendered_template = html_template.format(manager_state=manager_state, divs="\n".join(divs))
    with open(fname, "wt") as f:
        f.write(rendered_template)

def get_data_01():
    def create(frame: int, data: DataCollection):
        particles = data.create_particles(count=3)
        particles.create_property('Position', data=[(-0.06, 1.83, 0.81),(1.79, -0.88, -0.11),(-1.73, -0.77, -0.61)])
        type_property = particles.create_property('Particle Type')
        if len(type_property.types) == 0: 
            type_property.types.append(ParticleType(id=1, name='Red', color=(1,0,0)))
            type_property.types.append(ParticleType(id=2, name='Green', color=(0,1,0)))
            type_property.types.append(ParticleType(id=3, name='Blue', color=(0,0,1)))
        type_property[...] = [1, 2, 3]
        bonds = particles.create_bonds(count=3, vis_params={'width': 0.6})
        bonds.create_property('Topology', data=[(0,1),(1,2),(2,0)])
        cell_matrix = [[10,0,0,-5],[0,10,0,-5],[0,0,10,-5]]
        cell = data.create_cell(cell_matrix, pbc=(False, False, False))
        cell.vis.rendering_color = (1,1,1)
        cell.vis.line_width *= 2
    pipe = Pipeline(source = PythonSource(function = create))
    cell = pipe.compute().cell[...]
    pipe.modifiers.append(AffineTransformationModifier(transformation = [[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 3.0], [0.0, 0.0, 1.0, -cell[2,3]+7]]))
    # pipe.compute().particles.vis.rendering_quality = ovito.nonpublic.ParticleRenderingQuality.HighQuality
    return pipe, 0.7/cell[2,2]

def get_data_02():
    pipe = import_file('sample_data/mp-*-beta-quartz.POSCAR')
    def modify_pipeline_input(frame: int, data: DataCollection):
        data.particles_.particle_types_.type_by_id_(1).radius = 0.59
        data.particles_.particle_types_.type_by_id_(1).shape = ParticlesVis.Shape.Sphere
        data.particles_.particle_types_.type_by_id_(2).radius = 0.37
    pipe.modifiers.append(modify_pipeline_input)
    pipe.modifiers.append(ReplicateModifier(
        num_x = 2, 
        num_y = 2, 
        num_z = 2))
    pipe.modifiers.append(CreateBondsModifier(mode = CreateBondsModifier.Mode.VdWRadius))
    # pipe.compute().particles.vis.rendering_quality = ovito.nonpublic.ParticleRenderingQuality.HighQuality
    cell = pipe.source.data.cell[...]
    pipe.modifiers.append(AffineTransformationModifier(transformation = [[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 3.0], [0.0, 0.0, 1.0, -cell[2,3]+5]]))
    return pipe, 0.4/cell[2,2]

def get_data_03():
    pipe = import_file('sample_data/deform.30.ca')
    pipe.compute().dislocations.vis.line_width = 2.2
    cell = pipe.source.data.cell[...]
    pipe.modifiers.append(AffineTransformationModifier(transformation = [[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 3.0], [0.0, 0.0, 1.0, -cell[2,3]+5]]))
    return pipe, 3.5/cell[2,2]

def get_data_04():
    # Data import:
    pipeline = import_file('sample_data/tiny_nylon.data', atom_style = 'full')

    # Visual element initialization:
    pipeline.compute().particles.vis.scaling = 0.66
    pipeline.compute().particles.bonds.vis.width = 0.406
    pipeline.compute().cell.vis.enabled = False

    # Construct surface mesh:
    mod = ConstructSurfaceModifier(
        method = ConstructSurfaceModifier.Method.GaussianDensity, 
        grid_resolution = 100,
        radius_scaling = 1.65,
        isolevel = 0.604,
    )
    mod.vis.surface_color = (0.5843, 0.8666, 1.0)
    mod.vis.show_cap = False
    mod.vis.surface_transparency = 0.436
    pipeline.modifiers.append(mod)
    pipeline.modifiers.append(SliceModifier(distance = 8.6, operate_on = {'surfaces'}))
    pipeline.modifiers.append(AffineTransformationModifier(transformation = [[1.0, 0.0, 0.0, 0.0], [0.0, 1.0, 0.0, 0.0], [0.0, 0.0, 1.0, 5.0]]))
    return pipeline, 0.1

def get_data_05():
    pipeline = import_file('sample_data/dump.ellipsoid.gz', columns = ['Particle Identifier', 'Position.X', 'Position.Y', 'Position.Z', 'Particle Type', 'Orientation.X', 'Orientation.Y', 'Orientation.Z', 'Orientation.W', 'Aspherical Shape.X', 'Aspherical Shape.Y', 'Aspherical Shape.Z', 'Color.R', 'Color.G', 'Color.B'])
    pipeline.modifiers.append(ComputePropertyModifier(
        expressions = ('0', '0', '1'), 
        output_property = 'Force'))
    pipeline.modifiers.append(AffineTransformationModifier(
        target_cell = [[16.2865009, 0.0, 0.0, -8.0], [0.0, 16.2865009, 0.0, 0.0], [0.0, 0.0, 16.2865009, 0.0]], 
        relative_mode = False))
    data = pipeline.compute() # Evaluate new pipeline to gain access to visual elements associated with the imported data objects.
    data.particles.vis.radius = 1.3
    data.cell.vis.line_width = 0.04
    data.cell.vis.rendering_color = (1.0, 1.0, 1.0)
    data.particles.forces.vis.enabled = True
    data.particles.forces.vis.scaling = 2.8
    data.particles.forces.vis.flat_shading = False
    data.particles.forces.vis.width = 0.084
    data.particles.forces.vis.color_mapping_property = "Position.Z"
    data.particles.forces.vis.color_mapping_gradient = ColorCodingModifier.Magma()
    data.particles.forces.vis.color_mapping_interval = (0.0647601, 16.195)
    del data # Done accessing input DataCollection of pipeline.    
    return pipeline, 0.1

def get_data_06():
    pipeline = import_file('sample_data/glassy-spheres.dump.gz')
    pipeline.compute().particles.vis.rendering_quality = ovito.nonpublic.ParticleRenderingQuality.LowQuality
    mod = SliceModifier()
    mod.distance = 109.5
    mod.normal = (1.0, 1.0, 0.0)
    mod.inverse = True
    pipeline.modifiers.append(mod)
    pipeline.modifiers.append(AmbientOcclusionModifier())
    pipeline.modifiers.append(AffineTransformationModifier(
        target_cell = [[221.0, 0.0, 0.0, -110.0], [0.0, 221.0, 0.0, 0.0], [0.0, 0.0, 221.0, 0.0]], 
        relative_mode = False))
    pipeline.compute()
    return pipeline, 0.01

def get_data_07():
    pipeline = import_file('sample_data/dislocated_crystal.data', atom_style = 'atomic')
    pipeline.compute().cell.vis.enabled = False
    pipeline.compute().particles.vis.radius = 1.3
    pipeline.compute().particles.vis.scaling = 0.476
    # Manual modifications of the imported data objects:
    def modify_pipeline_input(frame: int, data: DataCollection):
        data.cell_.pbc = (True, True, False)
        data.particles_.particle_types_.type_by_id_(1).color = (0.0, 1.0, 0.15259000658988953)
        data.particles_.particle_types_.type_by_id_(1).name = 'Sr'
        data.particles_.particle_types_.type_by_id_(1).radius = 2.15000009537
        data.particles_.particle_types_.type_by_id_(2).color = (0.41777676343917847, 0.4448157548904419, 0.8210421800613403)
        data.particles_.particle_types_.type_by_id_(2).name = 'Ti'
        data.particles_.particle_types_.type_by_id_(2).radius = 1.47000002861
        data.particles_.particle_types_.type_by_id_(3).color = (1.0, 0.05098039284348488, 0.05098039284348488)
        data.particles_.particle_types_.type_by_id_(3).name = 'O'
        data.particles_.particle_types_.type_by_id_(3).radius = 0.740000009537
    pipeline.modifiers.append(modify_pipeline_input)
    pipeline.modifiers.append(SliceModifier(slab_width = 22.5))
    pipeline.modifiers.append(SliceModifier(distance = 63.37, normal = (0.0, 1.0, 0.0), slab_width = 26.0))
    pipeline.modifiers.append(ReplicateModifier(num_z = 2))
    pipeline.modifiers.append(SelectTypeModifier(types = {2}))
    mod = CreateBondsModifier(cutoff = 2.8)
    mod.vis.width = 0.07
    mod.vis.color = (0.6061798930168152, 0.5909208655357361, 0.6391546726226807)
    mod.vis.coloring_mode = BondsVis.ColoringMode.Uniform
    pipeline.modifiers.append(mod)
    mod = CoordinationPolyhedraModifier()
    mod.vis.surface_color = (0.9514305591583252, 0.9736018776893616, 0.958052933216095)
    mod.vis.highlight_edges = True
    pipeline.modifiers.append(mod)
    pipeline.modifiers.append(ClearSelectionModifier())
    return pipeline, 0.1

layout = ipywidgets.Layout(width='200px', height='200px')

def add_dataset(func, preview_mode=False):
    # Reset OVITO scene by removing all pipeline first.
    del ovito.scene.pipelines[:]

    # Call supplied function that sets up an OVITO pipeline. 
    # Function also returns a scale factor, which determines the apparent size of the 3d model in VR mode.
    pipe, sf = func()
    pipe.add_to_scene()

    # Create OVITO virtual viewport.
    vp = Viewport(type=Viewport.Type.Perspective, camera_dir=(0.5, 1.0, -0.4), preview_mode=preview_mode)
    vp.zoom_all()

    # Create Jupyter widget with VR mode enabled.
    return [vp.create_jupyter_widget(enable_vr=True, antialiasing=True, vr_scale=sf, layout=layout)]

def main():
    
    # Build list of Jupyter viewport widgets, one for each VR scene.
    widgets = []
    widgets += add_dataset(get_data_01, preview_mode=True)
    widgets += add_dataset(get_data_02)
    widgets += add_dataset(get_data_03)
    widgets += add_dataset(get_data_04)
    widgets += add_dataset(get_data_05, preview_mode=True)
    widgets += add_dataset(get_data_06)
    widgets += add_dataset(get_data_07)

    # Export widgets to a static self-contained HTML file.
    export_widgets("index.html", widgets)

if __name__ == "__main__":
    main()